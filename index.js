// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout() сработает один раз через указаный промежуток времени
// setInterval() будет срабатывать каждый раз через указаный интервал

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Сработает без задержки только после выполнения всех синхронных операций (алёрт, консоль-лог и тд)

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Цикл setInterval() будет работать бесконечно. Если он не нужен - его нужно выключить при помощи clearInterval(setInterval())

const startBtn = document.querySelector(".start-btn");
const stopBtn = document.querySelector(".stop-btn");
const timer = document.querySelector(".timer");
const images = document.querySelectorAll(".image-to-show");
const imagesArray = [...images];

timer.innerText = "3";

let timerId;
let index = 0;

timerId = setInterval(changeImage, 100);

function changeImage() {
  if (index === imagesArray.length) {
    clearInterval(this.timerId);
    index = 0;
    changeImage();
  }

  if (timer.innerText <= "3.00") {
    imagesArray[index].style.display = "block";
  }

  if (timer.innerText === "0.50") {
    imagesArray[index].classList.add("hide");
  }

  if (timer.innerText === "0.00") {
    clearInterval(this.timerId);
    imagesArray[index].style.display = "none";
    imagesArray[index].classList.remove("hide");
    timer.innerText = 3;
    index += +1;
    changeImage();
  }

  timer.innerText = (timer.innerText - 0.1).toFixed(2);
}

startBtn.addEventListener("click", () => {
  clearInterval(timerId);
  timerId = setInterval(changeImage, 100);
});

stopBtn.addEventListener("click", () => {
  clearInterval(timerId);
});
